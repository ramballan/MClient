package connections;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 * This callable class can try connect to direct Ip address and return true when connected, false when timeout 2 sec.
 */
public class DirectConnection implements Callable<Boolean>{
    private String IPAddress;
    private int remotePort;

    public DirectConnection(String IPAddress, int remotePort) {
        this.IPAddress = IPAddress;
        this.remotePort = remotePort;
    }

    @Override
    public Boolean call() throws Exception {
        SocketChannel sc = null;
        try {
            try {
                sc = SocketChannel.open();
                sc.configureBlocking(false);
                sc.connect(new InetSocketAddress(IPAddress,remotePort));
                int count = 0;
                while (count<2000){
                    if (sc.finishConnect()) return true;
                    TimeUnit.MILLISECONDS.sleep(1);
                    count++;
                }
                return false;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }finally {
                sc.close();
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
}
