package connections;

import javafx.scene.control.Label;
import java.io.IOException;
import java.net.*;
import java.util.Scanner;
import java.util.concurrent.Callable;

/**
 * this callable class find server with DatagramPacket.
 */
public class LocalNetConnection implements Callable<Boolean>{
    private static String findIP;
    @Override
    public Boolean call() throws Exception {
        MulticastSocket ds = null;
        ServerSocket ss = null;
        try{
            ds = new MulticastSocket(9999);
            String adr = InetAddress.getLocalHost().getHostAddress();
            StringBuilder sb = new StringBuilder();
            Scanner scaner = new Scanner(adr);
            scaner.useDelimiter("\\.");
            sb.append(scaner.next()+".");
            sb.append(scaner.next()+".");
            sb.append(scaner.next()+".");
            sb.append(255);
            byte[] buffer = new byte[1024];
            buffer = "Hello Server".getBytes();
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length, InetAddress.getByName(sb.toString()), 9999);
            ds.send(packet);
            ss = new ServerSocket(9998);
            ss.setSoTimeout(2000);
            Socket server = ss.accept();
            findIP = server.getInetAddress().getHostAddress();
            return true;
        }catch (IOException e){
            findIP = null;
            return false;
        }
        finally {
            ds.close();
            ss.close();
        }
    }

    public static String getFindIP() {
        return findIP;
    }
}
