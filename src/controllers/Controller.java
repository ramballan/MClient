package controllers;

import connections.DirectConnection;
import connections.LocalNetConnection;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.text.TextAlignment;
import objects.Command;
import other.TextFieldStringListener;
import other.WindowManager;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.util.concurrent.*;


public class Controller {
    private static String serverIP;
    private static final int REMOTEPORTFORCMD = 9997;
    public Label connectionResultLabel;
    public Tab sendMessageTab;
    public Tab sendPictureTab;
    public Button picturePreviewButton;
    public Button sendPictureButton;
    public TextField pictureTimeoutTextfield;
    public TextField pictureURLTextfield;
    public Button sendMessageButton;
    public Button messagePreviewButton;
    public TextField messageTimeoutTextField;
    public TextField messageTextField;
    private ExecutorService executorService = Executors.newCachedThreadPool();
    public CheckBox searchLocalCheckBox;
    public CheckBox searchCurrentCheckBox;
    public Button findInLocalButton;
    public Button findCurrentButton;
    public TextField IPAddr1;
    public TextField IPAddr2;
    public TextField IPAddr3;
    public TextField IPAddr4;
    private WindowManager windowManager;

    @FXML
    private void initialize() {
        findCurrentButton.setDisable(true);
        findInLocalButton.setDisable(true);
        IPAddr1.setDisable(true);
        IPAddr2.setDisable(true);
        IPAddr3.setDisable(true);
        IPAddr4.setDisable(true);
        IPAddr1.textProperty().addListener(new TextFieldStringListener(IPAddr1));
        IPAddr2.textProperty().addListener(new TextFieldStringListener(IPAddr2));
        IPAddr3.textProperty().addListener(new TextFieldStringListener(IPAddr3));
        IPAddr4.textProperty().addListener(new TextFieldStringListener(IPAddr4));
        pictureTimeoutTextfield.textProperty().addListener(new TextFieldStringListener(pictureTimeoutTextfield,1,300));
        messageTimeoutTextField.textProperty().addListener(new TextFieldStringListener(pictureTimeoutTextfield,1,300));
        connectionResultLabel.setWrapText(true);
        connectionResultLabel.setTextAlignment(TextAlignment.CENTER);
        windowManager = new WindowManager();
    }


    public void checkBoxChange(ActionEvent actionEvent) {
        connectionResultLabel.setText("");
        setDisableTabs(true);
        Object source = actionEvent.getSource();
        if (!(source instanceof CheckBox)) {
            return;
        }
        CheckBox checkBox = (CheckBox) source;
        boolean localDisable = true, currentDisable = true;
        switch (checkBox.getId()) {
            case "searchLocalCheckBox":
                localDisable = false;
                currentDisable = true;
                break;
            case "searchCurrentCheckBox":
                localDisable = true;
                currentDisable = false;
                break;
        }
        searchLocalCheckBox.setSelected(currentDisable);
        findInLocalButton.setDisable(localDisable);
        searchCurrentCheckBox.setSelected(localDisable);
        findCurrentButton.setDisable(currentDisable);
        IPAddr1.setDisable(currentDisable);
        IPAddr2.setDisable(currentDisable);
        IPAddr3.setDisable(currentDisable);
        IPAddr4.setDisable(currentDisable);
    }


    public void directConnection(ActionEvent actionEvent) {
        String style;
        StringBuilder IPAddress = new StringBuilder();
        IPAddress.append(IPAddr1.getText()).append(".");
        IPAddress.append(IPAddr2.getText()).append(".");
        IPAddress.append(IPAddr3.getText()).append(".");
        IPAddress.append(IPAddr4.getText());
        Future<Boolean> future = executorService.submit(new DirectConnection(IPAddress.toString(), REMOTEPORTFORCMD));
        boolean connected = false;
        try {
            connected = future.get(2, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
        }
        if (connected){
            serverIP = IPAddress.toString();
            setDisableTabs(false);
            style = "-fx-background-color: aquamarine";
        }else {
            System.out.println("error while connecting to:"+IPAddress.toString());
            style = "-fx-background-color: lightpink";
            setDisableTabs(true);
        }
        IPAddr1.setStyle(style);
        IPAddr2.setStyle(style);
        IPAddr3.setStyle(style);
        IPAddr4.setStyle(style);
    }

    public void localNetConnection(ActionEvent actionEvent) {
        connectionResultLabel.setText("");
        Future<Boolean> future = executorService.submit(new LocalNetConnection());
        boolean connected = false;
        try {
            connected = future.get(2, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {

        }
        if (connected){
            connectionResultLabel.setText("Сервер обнаружен: "+LocalNetConnection.getFindIP());
            serverIP = LocalNetConnection.getFindIP();
            setDisableTabs(false);
        }else {
            connectionResultLabel.setText("Сервер не обнаружен");
            setDisableTabs(true);
        }
    }
    private void setDisableTabs(boolean flag){
        sendPictureTab.setDisable(flag);
        sendMessageTab.setDisable(flag);
    }

    public void picturePreview(ActionEvent actionEvent) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL pictureURL = new URL(pictureURLTextfield.getText());
                    int timeout = Integer.parseInt(pictureTimeoutTextfield.getText());
                    windowManager.showPicture(pictureURL,timeout);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void sendPicture(ActionEvent actionEvent) {
        Socket connection;
        try {
            connection = new Socket(serverIP,9997);
            ObjectOutputStream outputStream = new ObjectOutputStream(connection.getOutputStream());
            outputStream.flush();
            URL pictureURL = new URL(pictureURLTextfield.getText());
            int timeout = Integer.parseInt(pictureTimeoutTextfield.getText());
            Command cmd = new Command(pictureURL,timeout);
            outputStream.writeObject(cmd);
            connection.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void messagePreview(ActionEvent actionEvent) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String message = messageTextField.getText();
                int timeout = Integer.parseInt(messageTimeoutTextField.getText());
                windowManager.showText(message,timeout);
            }
        }).start();
    }

    public void sendMessage(ActionEvent actionEvent) {
        Socket connection;
        try {
            connection = new Socket(serverIP,9997);
            ObjectOutputStream outputStream = new ObjectOutputStream(connection.getOutputStream());
            outputStream.flush();
            String message = messageTextField.getText();
            int timeout = Integer.parseInt(messageTimeoutTextField.getText());
            Command cmd = new Command(message,timeout);
            outputStream.writeObject(cmd);
            connection.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}