package other;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.*;



/**
 * This class can hold link on textfield, that will controlled by this listener
 */
public class TextFieldStringListener implements ChangeListener<String> {
    TextField textField;
    int min;
    int max;

    public TextFieldStringListener(TextField textField, int min, int max) {
        this.textField = textField;
        this.min = min;
        this.max = max;
    }

    public TextFieldStringListener(TextField textField) {
        this.textField = textField;
        min = 0;
        max = 255;
    }

    //control: only digits can be in textfield. Value can be from 0 to 255
    @Override
    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        String result = newValue;
        if (!result.matches("\\d*")) {
            result = result.replaceAll("[^\\d]", "");
        }
        if (!result.matches("^[0]")) {
            result = result.replaceAll("^[0]", "");
        }
        if (!(result.equals(""))){
            int val = Integer.valueOf(result);
            if ((val<min)){
                textField.setText(String.valueOf(min));
                return;
            }
            if ((val>max)){
                textField.setText(String.valueOf(max));
                return;
            }
            textField.setText(result);
        }
    }
}
